export default class Meal {

	constructor(data = {}) {

		this.id = data.id
		this.description = data.description
		this.imageUrl = data.imageUrl
		this.checkedMealOptions = data.checkedMealOptions || []
	}
}