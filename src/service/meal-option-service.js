import Observable from '../util/observable'
import MealOption from '../model/mealOption'

let mealOptions = [
	{
		id: "1",
		name : "White Bread",
	},
	{
		id: "2",
		name : "Chips"
	},
	{
		id: "3",
		name : "Brown Bread"
	},
	{
		id: "4",
		name : "Coke"
	},
	{
		id: "5",
		name : "Coke lite"
	},
	{
		id: "6",
		name : "Backed Potato's"
	}		
]

class MealOptionService extends Observable {

	constructor() {
		super()		
	}

	getAllMealOptions() {
		// retrieve data from firebase
		return mealOptions.map(option => new MealOption(option));
	}

	getFiteredMealOptions(toRemove){
		let availableMealOptions = mealOptions.filter(function(obj) {
				return !toRemove.some(function(obj2) {
					return obj.id == obj2.id;
			});
		});
		
		return availableMealOptions.map(option => new MealOption(option));
	}
}

export default new MealOptionService();