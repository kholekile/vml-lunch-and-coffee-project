import Observable from '../util/observable'
import Meal from '../model/meal'

let meals = [
	{	
		id : "12345",
		description : "1/4 Chicken Meal",
		imageUrl : "https://media-cdn.tripadvisor.com/media/photo-s/0f/81/98/b4/4-chicken-meal.jpg",
		checkedMealOptions: [
			{
				id: "1",
				name : "White Bread",
			},
			{
				id: "2",
				name : "Chips"
			}
		]
	},
	{	
		id : "567891",
		description : "1/2 Chicken Meal",
		imageUrl : "https://media-cdn.tripadvisor.com/media/photo-s/0f/81/98/b4/4-chicken-meal.jpg",
		checkedMealOptions: []
	},
	{	
		id : "12131",
		description : "Full Chicken + 2 sharing sides",
		imageUrl : "https://media-cdn.tripadvisor.com/media/photo-s/0f/81/98/b4/4-chicken-meal.jpg",
		checkedMealOptions: []
	},
	{	
		id : "14156",
		description : "1/2 Chicken + any Single Side",
		imageUrl : "https://media-cdn.tripadvisor.com/media/photo-s/0f/81/98/b4/4-chicken-meal.jpg",
		checkedMealOptions: []
	},
	{	
		id : "17181",
		description : "Chicken Burger",
		imageUrl : "https://media-cdn.tripadvisor.com/media/photo-s/0f/81/98/b4/4-chicken-meal.jpg",
		checkedMealOptions: [
			{
				id: "3",
				name : "Brown Bread"
			},
			{
				id: "4",
				name : "Coke"
			}
		]
	}
]

class MealService extends Observable {
	constructor() {
		super()		
	}

	getAllMeals() {
		// retrieve data from firebase
		return meals.map(meal => new Meal(meal));
	}

	deleteMeal(id) {
		// need to delete from firebase
		var index = meals.map(x => {
			return x.id;
		}).indexOf(id);

		meals.splice(index, 1);

		return meals.map(meal => new Meal(meal));
	}

	getMeal(id) {
		// need to get item by id from firebase
		let item = meals.filter( meal => meal.id === id );
		
		return new Meal(item[0]);
	}

	addMeal(meal){
		
		var arrayLength = meals.length + 1;
		
		var mealObject = {
			id: arrayLength.toString(),
			description : meal.itemDescription,
			checkedMealOptions : meal.checkedMealOptions,
			imageUrl : "https://media-cdn.tripadvisor.com/media/photo-s/0f/81/98/b4/4-chicken-meal.jpg"
		}

		meals.push(mealObject); 
		
		return meals.map(meal => new Meal(meal));  
	}

	update(meal){

		var foundIndex = meals.findIndex(x => x.id == meal.id);
		meals[foundIndex] = meal;
		var result = this.getMeal(meal.id);
		return result;
	}

}

export default new MealService();