import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import MealOptions from './views/MealOptions.vue'
import Members from './views/Members.vue'
import OrderManagement from './views/OrderManagement.vue'
import MealCalender from './views/MealCalender.vue'
import MealDetailPage from './views/MealDetailPage.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      icon: "fastfood",
      display: true,
      component: Dashboard
    },
     {
      path: '/meal-options',
      name: 'Meal Options',
      icon: "local_grocery_store",
      display: true,
      component: MealOptions
    },
    {
      path: '/members',
      name: 'Members',
      icon: "people",
      display: true,
      component: Members
    },
    {
      path: '/order-management',
      name: 'Order Management',
      icon: "add_shopping_cart",
      display: true,
      component: OrderManagement
    },
    {
      path: '/meal-calender',
      name: 'Meal Calender',
      icon: "calendar_today",
      display: true,
      component: MealCalender
    },
    {
      path: '/meal/:mealId',
      name: 'Meal Detail',
      icon: "",
      display: false,
      component: MealDetailPage,
      props: true
    }
  ]
})
